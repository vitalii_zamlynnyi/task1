package logout;

import base_test.BaseTest;
import elements.NavigationMenu;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.DataProviderPool;

public class UiLogoutTest extends BaseTest {

    @Test(dataProvider = "UserCredentials", dataProviderClass = DataProviderPool.class)
    public void uiLogoutTest(String email, String password) {
        NavigationMenu navigationMenu = new NavigationMenu();

        navigationMenu
                .clickLogin()
                .setEmail(email)
                .setPassword(password)
                .submitLogin()
                .navigationMenu
                .clickLogout();

        Assert.assertTrue(navigationMenu.isLoginLinkDisplayed(), "The user is not logged out");
    }
}
