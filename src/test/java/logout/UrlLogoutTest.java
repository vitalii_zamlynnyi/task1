package logout;

import base_test.BaseTest;
import elements.NavigationMenu;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.DataProviderPool;

public class UrlLogoutTest extends BaseTest {

    @Test(dataProvider = "UserCredentials", dataProviderClass = DataProviderPool.class)
    public void urlLogoutTest(String email, String password) {
        NavigationMenu navigationMenu = new NavigationMenu();

        navigationMenu
                .clickLogin()
                .setEmail(email)
                .setPassword(password)
                .submitLogin()
                .urlLogout();

        Assert.assertTrue(navigationMenu.isLoginLinkDisplayed(), "The user is not logged out");
    }
}
