package utils;

import org.testng.annotations.DataProvider;

public class DataProviderPool {
    @DataProvider(name = "UserCredentials")
    public static Object[][] getLoginData() {
        return new Object[][]{
                {"test7671564@gmail.com", "test7671564"}
        };
    }
}
