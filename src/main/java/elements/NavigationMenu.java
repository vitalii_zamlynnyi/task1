package elements;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;
import pages.LoginPage;
import ru.yandex.qatools.htmlelements.element.Link;

public class NavigationMenu extends BasePage {

    @FindBy(className = "nav")
    private WebElement navigationMenu;

    @FindBy(className = "logout")
    private Link logout;
    @FindBy(className = "login")
    private Link login;
    @FindBy(className = "account")
    private Link account;


    public NavigationMenu() {
        super();
        waitForVisibility(navigationMenu);
    }

    @Step
    public LoginPage clickLogin() {
        click(login);
        return new LoginPage();
    }

    @Step
    public NavigationMenu clickLogout() {
        click(logout);
        return this;
    }

    public boolean isLoginLinkDisplayed() {
        return isElementDisplayed(login);
    }

    public boolean isAccountLinkDisplayed() {
        return isElementDisplayed(account);
    }
}
