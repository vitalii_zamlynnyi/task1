package driver;

import Constants.Browser;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.SkipException;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DriverFactory {

    public static WebDriver initDriver(String browser) {
        switch (browser) {
            case Browser.FIREFOX:
                System.setProperty("webdriver.gecko.driver", getResourceByName("/geckodriver.exe"));
                return new FirefoxDriver();
            case Browser.IE:
                System.setProperty("webdriver.ie.driver", getResourceByName("/IEDriverServer.exe"));
                InternetExplorerOptions ieOptions = new InternetExplorerOptions().setPageLoadStrategy(PageLoadStrategy.NONE);
                ieOptions.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
                ieOptions.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
                return new InternetExplorerDriver(ieOptions);
            case Browser.EDGE:
            case "MicrosoftEdge":
                System.setProperty("webdriver.edge.driver", getResourceByName("/MicrosoftWebDriver.exe"));
                return new EdgeDriver();
            case Browser.CHROME:
            default:
                System.setProperty("webdriver.chrome.driver", getResourceByName("/chromedriver.exe"));
                ChromeOptions options = new ChromeOptions();
                return new ChromeDriver(options);
        }
    }

    /**
     * @param resourceName The name of the resource (e.g. '/chromedriver')
     * @return Path to a resource
     */
    private static String getResourceByName(String resourceName) {
        try {
            return new File(DriverFactory.class.getResource(resourceName).toURI()).getPath();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static WebDriver initDriver(String browser, String gridUrl) {
        DesiredCapabilities capabilities;
        switch (browser) {
            case Browser.FIREFOX:
                capabilities = DesiredCapabilities.firefox();
                break;
            case Browser.IE:
                capabilities = DesiredCapabilities.internetExplorer();
                capabilities.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
                capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
                break;
            case Browser.EDGE:
                capabilities = DesiredCapabilities.edge();
                break;
            case Browser.CHROME:
            default:
                capabilities = DesiredCapabilities.chrome();
                break;
        }

        try {
            return new RemoteWebDriver(new URL(gridUrl), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new SkipException("Unable to create RemoteWebDriver instance!");
        }
    }

}
