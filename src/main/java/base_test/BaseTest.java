package base_test;

import driver.DriverFactory;
import listeners.TestListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

@Listeners(TestListener.class)
public abstract class BaseTest {
    private static Logger LOG = LogManager.getLogger(BaseTest.class.getName());

    private static final ThreadLocal<WebDriver> DRIVER = new ThreadLocal<>();
    private WebDriver driver;

    public static WebDriver getDriver() {
        return DRIVER.get();
    }

    public static String baseUrl = "http://automationpractice.com/index.php";


    @BeforeMethod
    @Parameters({"port"})
    public void setUp(@Optional String port) {
        String gridUrl = String.format("%s:%s/wd/hub", System.getProperty("gridUrl"), port);
        String browser = System.getProperty("browser");
        LOG.info("Browser: {}", browser);

        driver = System.getProperty("runLocally").equalsIgnoreCase("true") ?
                DriverFactory.initDriver(browser) : DriverFactory.initDriver(browser, gridUrl);

        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        DRIVER.set(driver);

        driver.navigate().to(baseUrl);
    }

    @AfterMethod
    public void tearDown() {
        if (DRIVER.get() != null) {
            DRIVER.get().quit();
            DRIVER.remove();
        }
    }
}
