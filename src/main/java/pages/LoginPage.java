package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Form;
import ru.yandex.qatools.htmlelements.element.TextInput;

public class LoginPage extends MainPage {
    @FindBy(id = "login_form")
    private Form loginForm;

    @FindBy(id = "email")
    private TextInput email;
    @FindBy(id = "passwd")
    private TextInput password;
    @FindBy(id = "SubmitLogin")
    private Button submitLogin;

    public LoginPage() {
        super();
        waitForVisibility(loginForm);
    }

    @Step
    public LoginPage setEmail(String email) {
        this.email.sendKeys(email);
        return this;
    }

    @Step
    public LoginPage setPassword(String password) {
        this.password.sendKeys(password);
        return this;
    }

    @Step
    public MainPage submitLogin() {
        click(submitLogin);
        return new MainPage();
    }
}
