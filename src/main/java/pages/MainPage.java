package pages;

import elements.NavigationMenu;
import io.qameta.allure.Step;

public class MainPage extends BasePage {

    public NavigationMenu navigationMenu = new NavigationMenu();

    public String logoutUrl = "?mylogout=";

    @Step
    public void urlLogout() {
        relativeNavigateTo(logoutUrl);
    }
}
