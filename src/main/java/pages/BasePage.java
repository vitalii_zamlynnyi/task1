package pages;

import base_test.BaseTest;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

public abstract class BasePage {
    protected WebDriver driver;
    protected WebDriverWait wait;

    public static int TIMEOUT = 30;

    public WebDriver getDriver() {
        return driver;
    }

    public BasePage() {
        driver = BaseTest.getDriver();
        wait = new WebDriverWait(driver, TIMEOUT);
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(getDriver())), this);
    }

    @Step
    public WebElement waitForVisibility(WebElement element) {
        return wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.visibilityOf(element));
    }

    @Step
    protected WebElement waitToBeClickable(WebElement element) {
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    @Step
    protected void click(WebElement element) {
        waitToBeClickable(element).click();
    }

    @Step
    public void relativeNavigateTo(String param) {
        getDriver().navigate().to(BaseTest.baseUrl + param);
    }

    protected boolean isElementDisplayed(WebElement element) {
        try {
            return waitForVisibility(element).isDisplayed();
        } catch (TimeoutException ignored) {
            return false;
        }
    }
}
