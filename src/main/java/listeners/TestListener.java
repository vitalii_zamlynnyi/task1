package listeners;

import base_test.BaseTest;
import io.qameta.allure.Attachment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import java.util.Arrays;

public class TestListener extends TestListenerAdapter implements ITestListener {

    private static Logger Log = LogManager.getLogger(TestListener.class.getName());

    @Override
    public void onTestFailure(ITestResult result) {
        Log.error("Test has failed. Test case - " + result.getMethod().getMethodName() +
                " has failed due to reason:  " + result.getThrowable());
        attachScreenshot();
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] attachScreenshot() {
        byte[] screenshotAs = null;
        try {
            screenshotAs = ((TakesScreenshot) BaseTest.getDriver()).getScreenshotAs(OutputType.BYTES);
        } catch (Exception e) {
            fail(e);
        }
        return screenshotAs;
    }

    @Attachment(value = "Unable to save screenshot")
    private String fail(Exception e) {
        return String.format("%s\n%s", e.getMessage(), Arrays.toString(e.getStackTrace()));
    }
}
