package Constants;

public interface Browser {
    String CHROME = "chrome";
    String IE = "ie";
    String EDGE = "edge";
    String FIREFOX = "firefox";
}
