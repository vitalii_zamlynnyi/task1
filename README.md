
## Running the tests

To run the tests you need to have Maven installed and maven system variable configured.
To generate a report you need to have Allure installed and allure system variable configured.

1. To run parallel tests locally with default parameters execute the command in the command line:
   ```mvn test```
2. You can specify a browser using the '-Dbrowser' argument:
   ```mvn test -Dbrowser=firefox```
3. To run the parallel test against a selenium grid server you'll need to set the '-DrunLocally' argument to 'false', specify a server URL and port:
   ```mvn test -DrunLocally=false -Dbrowser=firefox -Dport=5558 -DgridUrl=http://localhost```
4. To generate and open the allure report run the following in the CMD:
   ```start AllureServe.bat```

